#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <sys/mman.h>
#include <unistd.h>


void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {.next = next, .capacity = capacity_from_size(
            block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
struct region alloc_region(void const *addr, size_t query) {
    size_t actual_size = region_actual_size(size_from_capacity((block_capacity) {query}).bytes);
    void *new_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);
    if (new_addr == MAP_FAILED) {
        new_addr = map_pages(addr, actual_size, 0);
        if (new_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }
    int extend = new_addr == addr;
    block_init(new_addr, (block_size) {actual_size}, NULL);
    return (struct region) {.addr = new_addr, .size =actual_size, .extends = extend};

}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block == NULL) {
        return false;
    }
    if (!block_splittable(block, query)) {
        return false;
    }

    void* new_block = block->contents + query;

    size_t new_block_size = block->capacity.bytes - query;
    block_init(new_block, (block_size) {.bytes=new_block_size}, block->next);

    block->next = new_block;
    block->capacity.bytes = query;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static bool blocks_continuous(struct block_header const *fst, struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block == NULL) {
        return false;
    }
    if (block->next == NULL) {
        return false;
    }
    if (!mergeable(block, block->next)) {
        return false;
    }

    block->capacity.bytes += block->next->capacity.bytes + offsetof(struct block_header, contents);
    block->next = block->next->next;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    if (block == NULL) {
        return (struct block_search_result) {.type = BSR_CORRUPTED};
    }
    while (1) {
        while (try_merge_with_next(block));
        if (block->is_free && block->capacity.bytes > sz) {
            return (struct block_search_result) {.block=block, .type = BSR_FOUND_GOOD_BLOCK};
        }
        if (block == block->next) {
            return (struct block_search_result) {.type = BSR_CORRUPTED};
        }
        if (!try_merge_with_next(block)) {
            if (block->next == NULL) {
                return (struct block_search_result) {.block=block, .type = BSR_REACHED_END_NOT_FOUND};
            }
            block = block->next;
        }
    }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block, query);

    if (result.type != BSR_FOUND_GOOD_BLOCK) {
        return result;
    } else {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
        return result;
    }

}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) {
        return NULL;
    }
    struct region new_region = alloc_region(last->contents + last->capacity.bytes, query);
    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    if (last->is_free && new_region.extends) {
        last->capacity.bytes = last->capacity.bytes + new_region.size;
        return last;
    }
    last->next = new_region.addr;

    return new_region.addr;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    size_t actual_query = size_max(BLOCK_MIN_CAPACITY, query);
    struct block_search_result result_existing = try_memalloc_existing(actual_query, heap_start);

    if (result_existing.type == BSR_FOUND_GOOD_BLOCK) {
        return result_existing.block;
    }

    struct block_header *result_grow_heap = grow_heap(result_existing.block, actual_query);
    if (result_grow_heap == NULL) {
        return NULL;
    }
    struct block_search_result result_grow_heap_search = try_memalloc_existing(actual_query, result_grow_heap);
    if (result_grow_heap_search.type == BSR_FOUND_GOOD_BLOCK) {
        return result_grow_heap_search.block;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    /*
     * Idk it should also merge the previous block. Suppose we have a situation:
     * ----
     * block1: free
     * block2: taken
     * block3: free
     * ----
     * And we get a _free(block2) call. Then we have:
     * block1: free
     * block1+block3: free
     *
     * Kinda not good
     */
    while (try_merge_with_next(header));
}
