//
// Created by ruskaof on 10.12.22.
//

#ifndef MEMORY_ALLOCATOR_CUSTOM_TESTS_H
#define MEMORY_ALLOCATOR_CUSTOM_TESTS_H

#include <stddef.h>

void run_tests();

struct region alloc_region(void const *addr, size_t query);

#endif //MEMORY_ALLOCATOR_CUSTOM_TESTS_H
