#include "custom_tests.h"
#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"

#define test1_bl1_size 1024
#define test1_bl2_size 512

#define test_2_bl1_size 1<<7
#define test_2_bl2_size 1<<12

#define test_3_reg_size 1<<10
#define test_3_bl1_size 1<<14

#define initial_heap_size 1<<10





static void test_alloc_free(void* heap) {
    void* block1 = _malloc(test1_bl1_size);
    void* block2 = _malloc(test1_bl2_size);
    if (block1 == NULL || block2 == NULL) {
        debug("Unexpected NULL in _malloc");
    }

    printf("Heap size 2^20, block1: 1024, block2: 512\n");
    debug_heap(stdout, heap);
    printf("Free first block\n");
    _free(block1);
    debug_heap(stdout, heap);
    printf("Free second block\n");
    _free(block2);
    debug_heap(stdout, heap);

}

static void test_grow_heap_simple(void* heap) {
    printf("Running test 2\n");
    void* block1 = _malloc(test_2_bl1_size);
    printf("Add a block 2^7 size\n");
    debug_heap(stdout, heap);
    void* block2 = _malloc(test_2_bl2_size);
    printf("Add a block 2^12 size\n");
    printf("The heap must now change it's overall capacity\n");
    debug_heap(stdout, heap);


    _free(block1);
    _free(block2);
}

static void test_grow_heap_new_reg(void* heap) {
    printf("Alloc a region after our heap\n");
    alloc_region(heap + (1 << 13), test_3_reg_size);
    printf("Add a big block");
    _malloc(test_3_bl1_size);
    debug_heap(stdout, heap);
}

void run_tests() {
    printf("Init heap size 2^10,\n");
    void *heap = heap_init(initial_heap_size);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        printf("Unexpected NULL in heap_init\n");
    }
    test_alloc_free(heap);
    test_grow_heap_simple(heap);
    test_grow_heap_new_reg(heap);
}